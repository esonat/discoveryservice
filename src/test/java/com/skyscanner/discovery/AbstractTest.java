package com.skyscanner.discovery;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DiscoveryServiceApplication.class)
@WebAppConfiguration
@AutoConfigureMockMvc
public class AbstractTest {
    @Autowired
    protected MockMvc mvc;
    @Autowired
    WebApplicationContext webApplicationContext;
//
//    protected void setUp(){
//        mvc= MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
//    }

    protected String mapToJson(Object obj) throws Exception{
        ObjectMapper objectMapper=new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    protected <T> T mapFromJson(String json,Class<T> clazz) throws JsonParseException, JsonMappingException, IOException{
        ObjectMapper objectMapper=new ObjectMapper();
        return objectMapper.readValue(json,clazz);
    }
}
