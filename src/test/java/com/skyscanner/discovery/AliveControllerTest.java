package com.skyscanner.discovery;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.skyscanner.discovery.domain.RestAPI;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;

public class AliveControllerTest extends AbstractTest{

    @Test
    public void whenAliveMessageReceivedThenReturn204() throws Exception{
        String uri="/discovery-service/v1/alive";
        RestAPI restAPI=new RestAPI("flight-service","localhost",9000,true,"1111111");
        ObjectWriter ow=new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json=ow.writeValueAsString(restAPI);

        Assert.assertNotNull(webApplicationContext);
        Assert.assertNotNull(mvc);
        MvcResult mvcResult=mvc.perform(MockMvcRequestBuilders.post(uri).content(json).contentType(MediaType.APPLICATION_JSON)).andReturn();
        int status=mvcResult.getResponse().getStatus();
        assertEquals(204,status);
        String content=mvcResult.getResponse().getContentAsString();
    }

    @Test
    public void whenDifferentMessageSentThenReturn400() throws Exception{
        String uri="/discovery-service/v1/alive";
        String json="deneme";

        MvcResult mvcResult=mvc.perform(MockMvcRequestBuilders.post(uri).content(json).contentType(MediaType.APPLICATION_JSON)).andReturn();

        int status=mvcResult.getResponse().getStatus();
        assertEquals(400,status);
    }
}
