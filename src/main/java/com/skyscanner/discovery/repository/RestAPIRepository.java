package com.skyscanner.discovery.repository;

import com.skyscanner.discovery.domain.RestAPI;
import org.springframework.data.mongodb.repository.MongoRepository;

//@Query("{ 'host' :?0 }")
//@Query("{ 'port' : ?0")

//@Query("{  'connected' : ?0")

public interface RestAPIRepository extends MongoRepository<RestAPI,String> {

//    public List<RestAPI> findByHost(String host);
//
//    public List<RestAPI> findByPort(String port);
//
//    public List<RestAPI> findByConnected(boolean connected);

}
