package com.skyscanner.discovery.rest;

import com.skyscanner.discovery.datetime.TimestampUtil;
import com.skyscanner.discovery.domain.RestAPI;
import com.skyscanner.discovery.repository.RestAPIRepository;
import com.skyscanner.discovery.scheduled.AliveMessageTimedTask;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

@RestController
@RequestMapping("/discovery-service/v1")
public class AliveController {

    Logger logger=Logger.getLogger(AliveController.class);

//    @Autowired
//    private AnnotationConfigWebApplicationContext context;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private RestAPIRepository restAPIRepository;

    @Autowired
    private TaskScheduler taskScheduler;

//    @Autowired
//    private AliveMessageTimedTask aliveMessageTimedTask;

    private Timer timer;

    @RequestMapping(value="/alive",method = RequestMethod.POST, consumes="application/json")
    @ResponseStatus(value= HttpStatus.NO_CONTENT)
    public void checkAlive(@RequestBody RestAPI restAPI){
        logger.info("Alive message received from "+restAPI.toString());
        //taskScheduler.schedule(aliveMessageTimedTask,new Date());


        Optional<RestAPI> query=restAPIRepository.findById(restAPI.getId());

        restAPI.setConnected(true);
        restAPI.setTimestamp(TimestampUtil.GetTimestamp());

        //First Alive message
        if(!query.isPresent()){
            //Save the RestAPI as connected
            restAPIRepository.save(restAPI);

            Timer timer=new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {

                    AliveMessageTimedTask aliveMessageTimedTask=applicationContext.getBean(AliveMessageTimedTask.class);
                    aliveMessageTimedTask.setRestAPIId(restAPI.getId());

                    aliveMessageTimedTask.run();
                }
            },0,5000);


            //ScheduledExecutorService service= Executors.newSingleThreadScheduledExecutor();
            //service.scheduleAtFixedRate(aliveMessageTimedTask,0,5, TimeUnit.SECONDS);
            //taskScheduler.schedule(aliveMessageTimedTask,new Date());
            //Start timed task
            //new AliveMessageTimedTask(restAPI.getId(),5);
        }

        restAPIRepository.save(restAPI);
    }
}
