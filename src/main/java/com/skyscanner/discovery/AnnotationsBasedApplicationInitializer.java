//package com.skyscanner.discovery;
//
//import com.skyscanner.discovery.config.DiscoveryServiceBeanConfiguration;
//import org.springframework.web.context.AbstractContextLoaderInitializer;
//import org.springframework.web.context.WebApplicationContext;
//import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
//
//public class AnnotationsBasedApplicationInitializer extends AbstractContextLoaderInitializer {
//
//    @Override
//    protected WebApplicationContext createRootApplicationContext(){
//        AnnotationConfigWebApplicationContext rootContext=new AnnotationConfigWebApplicationContext();
//        rootContext.register(DiscoveryServiceBeanConfiguration.class);
//        return rootContext;
//    }
//}
