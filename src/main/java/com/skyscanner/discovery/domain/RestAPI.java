package com.skyscanner.discovery.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
public class RestAPI {
    @Id
    private String id;
    @Field("host")
    private String host;
    @Field("port")
    private int port;
    @Field("connected")
    private boolean connected;
    @Field("timestamp")
    private String timestamp;

    public RestAPI() {
    }

    public RestAPI(String id, String host, int port, boolean connected) {
        this.id = id;
        this.host = host;
        this.port = port;
        this.connected = connected;
    }

    public RestAPI(String id, String host, int port, boolean connected, String timestamp) {
        this.id = id;
        this.host = host;
        this.port = port;
        this.connected = connected;
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString(){
        return "Id:"+this.getId()+",host:"+this.getHost()+",port:"+this.getPort()+",timestamp:"+this.getTimestamp();
    }
}
