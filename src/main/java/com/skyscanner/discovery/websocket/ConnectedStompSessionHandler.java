//package com.skyscanner.discovery.websocket;
//
//import org.apache.log4j.Logger;
//import org.springframework.messaging.simp.stomp.StompCommand;
//import org.springframework.messaging.simp.stomp.StompHeaders;
//import org.springframework.messaging.simp.stomp.StompSession;
//import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
//
//import java.lang.reflect.Type;
//
//public class ConnectedStompSessionHandler extends StompSessionHandlerAdapter {
//    Logger logger=Logger.getLogger(ConnectedStompSessionHandler.class);
//
//
//    public ConnectedStompSessionHandler() {
//        super();
//    }
//
//    @Override
//    public Type getPayloadType(StompHeaders headers) {
//        return String.class;
//    }
//
//    @Override
//    public void handleFrame(StompHeaders headers, Object payload) {
//        //Message  msg=(Message) payload;
//        logger.info("Received :" +payload);
//
//    }
//
//    @Override
//    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
//        logger.info("Connected to websocket");
//    }
//
//    @Override
//    public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
//        logger.error("Got an exception",exception);
//    }
//
//    @Override
//    public void handleTransportError(StompSession session, Throwable exception) {
//        logger.error("Websocket transport error");
//    }
//}
