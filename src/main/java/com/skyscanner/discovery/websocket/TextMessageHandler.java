//package com.skyscanner.discovery.websocket;
//
//import org.apache.log4j.Logger;
//import org.springframework.web.socket.TextMessage;
//import org.springframework.web.socket.WebSocketSession;
//import org.springframework.web.socket.handler.TextWebSocketHandler;
//
//import java.io.IOException;
//
//public class TextMessageHandler extends TextWebSocketHandler {
//    Logger logger=Logger.getLogger(TextMessageHandler.class);
//
//    @Override
//    public void handleTextMessage(WebSocketSession session, TextMessage message) {
//        try{
//            session.sendMessage(message);
//        }catch (IOException e){
//            logger.error(e.getMessage());
//        }
//    }
//}
