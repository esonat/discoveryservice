//package com.skyscanner.discovery.websocket;
//
//
//import org.springframework.messaging.handler.annotation.DestinationVariable;
//import org.springframework.messaging.handler.annotation.MessageMapping;
//import org.springframework.messaging.handler.annotation.SendTo;
//import org.springframework.stereotype.Controller;
//
//@Controller
//public class ConnectedWebsocketController {
//
//    @MessageMapping("/connected/{topic}")
//    @SendTo("topic/messages")
//    public String send(@DestinationVariable("topic") String topic, String message){
//        return message;
//    }
//}
