package com.skyscanner.discovery;

import com.skyscanner.discovery.repository.RestAPIRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.Lifecycle;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.validation.constraints.NotNull;


@SpringBootApplication
public class DiscoveryServiceApplication {

    @Autowired
    private static ApplicationContext context;


    @Component
    public static class ApplicationLifecycle implements Lifecycle{

        @Autowired
        private RestAPIRepository repository;

        Logger logger=Logger.getLogger(ApplicationLifecycle.class);

        @Override
        public void start() {
            logger.info("Application start");
        }

        @Override
        public void stop() {
            logger.info("Application stop");
            repository.deleteAll();
        }

        @Override
        public boolean isRunning() {
            return true;
        }
    }

//    @Autowired
//    private RestAPIRepository repository;

    public static void main(String[] args){
        System.setProperty("server.servlet.context-path", "/");
        SpringApplication.run(DiscoveryServiceApplication.class,args);

    }

    @NotNull
    @Bean
    ServletListenerRegistrationBean<ServletContextListener> myServletListener() {
        ServletListenerRegistrationBean<ServletContextListener> srb =
                new ServletListenerRegistrationBean<>();
        srb.setListener(new DiscoveryServiceServletContextListener());
        return srb;
    }


    class DiscoveryServiceServletContextListener implements ServletContextListener {

        Logger logger= Logger.getLogger(com.skyscanner.discovery.config.DiscoveryServiceServletContextListener.class);


        @Override
        public void contextInitialized(ServletContextEvent sce) {
            logger.info("ServletContext initialized");
        }

        @Override
        public void contextDestroyed(ServletContextEvent sce){
            //logger.info("RestAPI repository all items deleted");
            //Optional<RestAPI> query=repository.findById("flight-service");
            //RestAPI restAPI=query.get();
            logger.info("ServletContext destroyed");
//            MongoClient mongoClient = new MongoClient();
//            DB database=mongoClient.getDB("skyscannerDiscoveryDB");
//            DBCollection collection=database.getCollection("restAPI");
//
//            BasicDBObject query=new BasicDBObject();
//            query.append("id","flight-service");
//            collection.remove(query);
//            //repository.deleteById("flight-service");
//            logger.info("flight-service deleted");

        }
    }
}


