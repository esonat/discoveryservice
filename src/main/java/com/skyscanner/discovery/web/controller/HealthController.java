package com.skyscanner.discovery.web.controller;

import com.skyscanner.discovery.domain.RestAPI;
import com.skyscanner.discovery.repository.RestAPIRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

//@Controller
//@RequestMapping("/discovery-service/v1")

@Controller
@RequestMapping("/discovery-service/v1")
public class HealthController{

    Logger logger= Logger.getLogger(HealthController.class);

    @Autowired
    private RestAPIRepository restAPIRepository;

    @RequestMapping(value = "/health",method = RequestMethod.GET)
    public String monitorHealth(Model model){
        logger.info("monitorHealth called");

        List<RestAPI> restAPIs=restAPIRepository.findAll();

        model.addAttribute("restAPIs",restAPIs);

        return "restapis";
    }
}