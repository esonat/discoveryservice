package com.skyscanner.discovery.config.web;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@PropertySource("classpath:application.properties")
//@ComponentScan("com.skyscanner.discovery.web.controller")
public class WebConfig {
    Logger logger=Logger.getLogger(WebConfig.class);

//    @Bean
//    public ContentNegotiatingViewResolver contentNegotiatingViewResolver() {
//        logger.info("contentNegotiatingViewResolver called");
//
//        ContentNegotiatingViewResolver contentNegotiatingViewResolver
//                = new ContentNegotiatingViewResolver();
//
//        BeanNameViewResolver beanNameViewResolver = new BeanNameViewResolver();
//        InternalResourceViewResolver internalResourceViewResolver = new InternalResourceViewResolver();
//
//        internalResourceViewResolver.setViewClass(org.springframework.web.servlet.view.JstlView.class);
//        internalResourceViewResolver.setPrefix("/WEB-INF/JSP/");
//        internalResourceViewResolver.setSuffix(".jsp");
//
//        List<ViewResolver> list = new ArrayList<ViewResolver>();
//        list.add(beanNameViewResolver);
//        list.add(internalResourceViewResolver);
//
//        contentNegotiatingViewResolver.setViewResolvers(list);
//
//        return contentNegotiatingViewResolver;
//    }

    @Bean
    public ResourceBundleMessageSource messageSource(){
        ResourceBundleMessageSource messageSource=new ResourceBundleMessageSource();

        messageSource.setBasename("messages");
        return messageSource;
    }

    @Bean
    public CommonsMultipartResolver multipartResolver(){
        CommonsMultipartResolver multipartResolver=new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(5000000);
        return multipartResolver;
    }

    @Bean
    public LocalValidatorFactoryBean validator(){
        LocalValidatorFactoryBean validator=new LocalValidatorFactoryBean();
        validator.setValidationMessageSource(messageSource());
        return validator;
    }


//    @Bean
//    public InternalResourceViewResolver viewResolver(){
//        InternalResourceViewResolver viewResolver=new InternalResourceViewResolver();
//
//        viewResolver.setViewClass(org.springframework.web.servlet.view.JstlView.class);
//        viewResolver.setPrefix("/WEB-INF/JSP/");
//        viewResolver.setSuffix(".jsp");
//
//        return viewResolver;
//    }

}
