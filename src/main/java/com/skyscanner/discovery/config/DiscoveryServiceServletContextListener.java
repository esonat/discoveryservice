package com.skyscanner.discovery.config;

import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class DiscoveryServiceServletContextListener implements ServletContextListener{

    Logger logger= Logger.getLogger(DiscoveryServiceServletContextListener.class);


    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.info("ServletContext initialized");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce){

    }
}
