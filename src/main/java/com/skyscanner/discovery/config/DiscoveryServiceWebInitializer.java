//package com.skyscanner.discovery.config;
//
//import com.skyscanner.discovery.config.web.WebConfig;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.core.annotation.Order;
//import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
//
//@Order(1)
//public class DiscoveryServiceWebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer{
//
//    Logger logger= LoggerFactory.getLogger(DiscoveryServiceWebInitializer.class);
//
//    protected Class<?>[] getRootConfigClasses(){
//        return new Class<?>[] {WebSecurityConfig.class,MongoConfig.class,SimpleMongoConfig.class};
//    }
//
//    protected Class<?>[] getServletConfigClasses(){
//        return new Class<?>[] {WebConfig.class};
//    }
//
//    protected String[] getServletMappings(){
//        logger.info("getServletMappings() called");
//        return new String[] {"/"};
//    }
//}