//package com.skyscanner.discovery.config;
//
//
//import com.skyscanner.discovery.config.web.WebConfig;
//import org.apache.log4j.Logger;
//import org.springframework.web.WebApplicationInitializer;
//import org.springframework.web.context.ContextLoaderListener;
//import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
//import org.springframework.web.servlet.DispatcherServlet;
//
//import javax.servlet.ServletContext;
//import javax.servlet.ServletRegistration;
//
//public class ServletInitializer implements WebApplicationInitializer{
//
//    Logger logger=Logger.getLogger(ServletInitializer.class);
//
//    @Override
//    public void onStartup(ServletContext container){
//        AnnotationConfigWebApplicationContext rootContext=new AnnotationConfigWebApplicationContext();
//        rootContext.register(DiscoveryServiceBeanConfiguration.class);
//
//        container.addListener(new ContextLoaderListener(rootContext));
//        AnnotationConfigWebApplicationContext dispatcherServlet=new AnnotationConfigWebApplicationContext();
//        dispatcherServlet.register(WebConfig.class);
//
//        ServletRegistration.Dynamic dispatcher=container.addServlet("dispatcher",new DispatcherServlet(dispatcherServlet));
//        dispatcher.setLoadOnStartup(1);
//        dispatcher.addMapping("/*");
//
//        logger.info("dispatcher set to /");
//    }
//}
////
////public class ServletInitializer extends SpringBootServletInitializer {
////
////    @Override
////    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
////        return application.sources(DiscoveryServiceApplication.class);
////    }
////
////}
