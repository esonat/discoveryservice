package com.skyscanner.discovery.config;

import com.mongodb.MongoClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "com.skyscanner.discovery.repository")
public class MongoConfig extends AbstractMongoConfiguration {

    @Override
    protected String getDatabaseName(){
        return "skyscannerDiscoveryDB";
    }

    @Override
    public MongoClient mongoClient(){
        return new MongoClient("127.0.0.1",27017);
    }

    @Override
    public String getMappingBasePackage(){
        return "com.skyscanner.discovery";
    }

    @Bean
    MongoTransactionManager transactionManager(MongoDbFactory dbFactory){
        return new MongoTransactionManager(dbFactory);
    }
}
