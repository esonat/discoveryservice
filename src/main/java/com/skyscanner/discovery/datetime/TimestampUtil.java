package com.skyscanner.discovery.datetime;

import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimestampUtil {

    static Logger logger=Logger.getLogger(TimestampUtil.class);


    public static String GetTimestamp(){
        Date date=new Date();
        long time=date.getTime();

        return new Timestamp(time).toString();
    }

    public static Timestamp convertToTimestamp(String str_timestamp){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
            Date parsedDate = dateFormat.parse(str_timestamp);
            Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());

            return timestamp;
        } catch(Exception e) { //this generic but you can control another types of exception
            logger.error(e.getMessage());
            return null;
        }
    }

    public static int getTimeDifferenceInSeconds(String str_timestamp1,String str_timestamp2){
        Timestamp timestamp1=convertToTimestamp(str_timestamp1);
        Timestamp timestamp2=convertToTimestamp(str_timestamp2);

        long milliseconds=timestamp1.getTime()-timestamp2.getTime();
        int seconds=(int)milliseconds/1000;

        return seconds;
    }
}
