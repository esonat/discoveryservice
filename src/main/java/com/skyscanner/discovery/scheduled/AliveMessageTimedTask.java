package com.skyscanner.discovery.scheduled;

import com.skyscanner.discovery.datetime.TimestampUtil;
import com.skyscanner.discovery.domain.RestAPI;
import com.skyscanner.discovery.repository.RestAPIRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AliveMessageTimedTask{


    @Autowired
    private RestAPIRepository restAPIRepository;

    String restAPIId;
    //Timer timer;
    Logger logger= Logger.getLogger(AliveMessageTimedTask.class);

    public AliveMessageTimedTask(){

    }

    public AliveMessageTimedTask(String restAPIId){
        this.restAPIId=restAPIId;
    //    timer=new Timer();
      //  timer.schedule(new AliveTask(),seconds*1000);
    }

    public String getRestAPIId() {
        return restAPIId;
    }

    public void setRestAPIId(String restAPIId) {
        this.restAPIId = restAPIId;
    }

    //class AliveTask extends TimerTask{
        //@Override
        public void run(){
            logger.info("Started.ID="+restAPIId);

            Optional<RestAPI> query=restAPIRepository.findById(restAPIId);
            if(query.isPresent()){
                RestAPI restAPI=query.get();
                String str_timestamp_now= TimestampUtil.GetTimestamp();
                String str_timestamp=restAPI.getTimestamp();

                int difference= TimestampUtil.getTimeDifferenceInSeconds(str_timestamp_now,str_timestamp);


                //If there was no alive message in the last 5 seconds
                if(difference>5){
                    restAPI.setConnected(false);
                    restAPIRepository.save(restAPI);
//                try {
//                    //send message to WebSocketHandler endpoint /connected
//                    WebSocketClient simpleWebSocketClient = new StandardWebSocketClient();
//                    List<Transport> transports = new ArrayList<>(1);
//                    transports.add(new WebSocketTransport(simpleWebSocketClient));
//                    SockJsClient sockJsClient = new SockJsClient(transports);
//
//                    WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
//
//                    stompClient.setMessageConverter(new MappingJackson2MessageConverter());
//
//                    String url = "ws://localhost:8080/connected";
//                    StompSessionHandler sessionHandler = new ConnectedStompSessionHandler();
//                    StompSession session = stompClient.connect(url, sessionHandler).get();
//
//                    session.send("/app/connected/"+String.valueOf(restAPI.getId()),"false");
//
//                }catch(InterruptedException e){
//                    logger.error(e.getMessage());
//                }catch (ExecutionException e){
//                    logger.error(e.getMessage());
//                }

                }
            }

        }
}


