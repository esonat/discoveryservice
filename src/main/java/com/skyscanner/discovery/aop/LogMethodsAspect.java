package com.skyscanner.discovery.aop;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;


@Component
@Aspect
public class LogMethodsAspect {

    @Before("execution(* com.skyscanner.discovery..*.*(..))")
    public void logMethod(JoinPoint joinPoint) throws Throwable{
        final Logger logger = Logger.getLogger(joinPoint.getTarget().getClass().getName());
        logger.info("Executing "+joinPoint.getSignature().getName());
    }
}
